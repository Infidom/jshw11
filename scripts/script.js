const icons = document.querySelectorAll('.fas');

icons.forEach(function(icon) {
    icon.onclick = function() {
        let target = this.getAttribute('data-target');
        let input = document.querySelector(target);
        if (input.getAttribute('type') === 'password') {
            input.setAttribute('type', 'text');
            icon.classList.remove("active");
            icon.classList.replace("fa-eye", "fa-eye-slash");
        } else {
            input.setAttribute('type', 'password');
            icon.classList.add("active");
            icon.classList.replace("fa-eye-slash", "fa-eye");
        }
    }
});

function checkIfMatching() {
    let inputPassword = document.getElementById('input-password');
    let confirmPassword = document.getElementById('confirm-password');
    if (inputPassword.value == '' || confirmPassword.value == '') {
        document.getElementById('error').innerText = 'Введіть пароль';
    } else {
        if (inputPassword.value !== confirmPassword.value) {
            document.getElementById('error').innerText = 'Потрібно ввести однакові значення';
        } else {
            alert('You are welcome');
            document.getElementById('error').innerText = '';
        }
    }
}